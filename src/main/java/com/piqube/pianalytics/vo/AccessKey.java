package com.piqube.pianalytics.vo;

/**
 * @author jenefar
 */
public enum AccessKey {
    DEVELOPMENT,
    PRODUCTION
}
