package com.piqube.pianalytics.vo;

/**
 * @author jenefar
 */
public class AccessKeyVO {
    private Boolean isTrail;
    private Boolean isEnabled;
    private Boolean generateKey;

	public Boolean getIsTrail() {
		return isTrail;
	}

	public void setIsTrail(Boolean isTrail) {
		this.isTrail = isTrail;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Boolean getGenerateKey() {
		return generateKey;
	}

	public void setGenerateKey(Boolean generateKey) {
		this.generateKey = generateKey;
	}
    
	

    
}
