package com.piqube.pianalytics.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jenefar
 */
@Configuration
@EnableAutoConfiguration
@EnableRedisHttpSession
@RestController
@ComponentScan("com.piqube.pianalytics")
@EnableJpaRepositories("com.piqube.pianalytics.repository")
public class BootUserService extends WebSecurityConfigurerAdapter{

    public static void main(String[] args) {

        SpringApplication.run(BootUserService.class, args);
    }

    @Override
    protected  void configure(HttpSecurity http) throws Exception{
        http.httpBasic().disable();
        http.csrf().disable();
    }

    @Bean
    HeaderHttpSessionStrategy sessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }

}
