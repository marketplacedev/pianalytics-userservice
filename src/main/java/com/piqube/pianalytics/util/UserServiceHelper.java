package com.piqube.pianalytics.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import com.piqube.pianalytics.vo.UserVO;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserServiceHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceHelper.class);
    private static int workload = 12;
    Pattern letter = Pattern.compile("[a-zA-z]");
    Pattern digit = Pattern.compile("[0-9]");
    Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
    Pattern eight = Pattern.compile (".{8}");

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    public  boolean passwordMatcher(String password) {
        //Matcher hasLetter = letter.matcher(password);
        Object hasDigit=digit.matcher(password).find();
        Object hasSpecial=special.matcher(password).find();
        Object hasEight=eight.matcher(password).find();
        if(hasDigit.equals(true)   && hasSpecial.equals(true)
                &&  hasEight.equals(true)) {
            return true;
        }else{
            return false;
        }
    }
    public String getLoggedInUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        LOGGER.debug("Logged in user is:{}",username);
        return username;
    }


    public  List<String> validateAdminSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();
        Pattern phoneNumberPattern = Pattern.compile("^\\d{8,11}$");

        if (userVO.getUsername() == null)
            result.add("Please enter the Email");
        else {
            boolean validity = isValidEmailAddress(userVO.getUsername());
            if (!validity)
                result.add("Please enter a valid Email");
            if(userVO.getCompanyCategory().equals("COMPANY")) {
                List<String> invalidEmailDomainList = Arrays.asList(userVO.getInvalidEmailDomains().split("\\s*,\\s*"));
                for (String invalidDomain : invalidEmailDomainList) {
                    if (userVO.getUsername().toLowerCase().contains(invalidDomain))
                        result.add("Please enter a Corporate Email address");
                }
            }
        }
        if (userVO.getPassword() == null)
            result.add("Please enter the Password");
        else if (!passwordMatcher(userVO.getPassword()))
            result.add("You password should be of at least 8 characters with one number and one special character\n");

        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else if (!phoneNumberPattern.matcher(userVO.getPhoneNumber()).find())
            result.add("Please enter a valid PhoneNumber");

        if (userVO.getCompanyName() == null)
            result.add("Please enter the CompanyName");
        if (userVO.getCompanySize() == null)
            result.add("Please enter the Company size");
        if (userVO.getCompanyCategory() == null)
            result.add("Please enter the CompanyCategory");
       /* if (userVO.getCompanyHeadQuarters() == null)
            result.add("Please enter the CompanyHeadQuarters");*/
        if (userVO.getCompanyLocation() == null || userVO.getCompanyLocation().isEmpty())
            result.add("Please enter the CompanyLocation");

        return result;

    }

    public  List<String> validateSubUserSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();

        if (userVO.getPassword() == null)
            result.add("Please enter the Password");
        else if (!passwordMatcher(userVO.getPassword()))
            result.add("You password should be of at least 8 characters with one number and one special character\n");
        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else {
            Pattern pattern = Pattern.compile("^\\d{8,11}$");
            if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                result.add("Please enter a valid PhoneNumber");
            }
        }
        return result;

    }

    public Map<String, Integer> setPreferences(Map<String, String> preferences) {
        Map<String, Integer> result = new HashMap<String, Integer>();
        Iterator iter = preferences.entrySet().iterator();

        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            result.put("IND " + entry.getValue(), Integer.valueOf( entry.getKey().toString()));
        }
        return result;
    }
    
    public List<String> getJobStatuses(){
        List statusList=new ArrayList();
        
        statusList.add("OFFER_ABORTED");
        return statusList;
    }
    
    public List<String> getAllocationStatuses(){
        List statusList=new ArrayList();
        
        statusList.add("REJECTED");
        return statusList;
    }
    public static String hashPassword(String password_plaintext) {
		String salt = BCrypt.gensalt(workload);
		String hashed_password = BCrypt.hashpw(password_plaintext, salt);

		return(hashed_password);
	}
	
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;

		if(null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

		return(password_verified);
	}
	 public String generatePassword() {
	        String chars = "0123456789";

	        final int PW_LENGTH = 6;
	        Random rnd = new SecureRandom();
	        StringBuilder pass = new StringBuilder();
	        for (int i = 0; i < PW_LENGTH; i++)
	            pass.append(chars.charAt(rnd.nextInt(chars.length())));
	        return pass.toString();
	    }  

	 
	 
public  List<String> validateAccount(UserVO userVO) {
 List<String> result = new ArrayList<String>();
   /* Pattern phoneNumberPattern = Pattern.compile("^\\d{8,11}$");
*/
    if (userVO.getPassword() == null)
     result.add("Please enter the Password");
 else if (!passwordMatcher(userVO.getPassword()))
     result.add("You password should be of at least 8 characters with one number and one special character\n");

 if (userVO.getFirstName() == null)
     result.add("Please enter the FirstName");
 if (userVO.getLastName() == null)
     result.add("Please enter the LastName");
 if (userVO.getTimezone() == null)
     result.add("Please enter the timezone");

   /* if (userVO.getPhoneNumber() == null)
        result.add("Please enter the  PhoneNumber");
    else if (!phoneNumberPattern.matcher(userVO.getPhoneNumber()).find())
        result.add("Please enter a valid PhoneNumber");*/
 return result;

}


    public String getEmailDomain(String a)
    {
        String b = a.substring(a.indexOf('@') + 1);
        return b;
    }
    
    public  List<String> currentvalidateAdminSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();

        Pattern phoneNumberPattern = Pattern.compile("^\\d{8,11}$");

        if (userVO.getUsername() == null) {
            result.add("Please enter the Email");
        } else {
            boolean validity = isValidEmailAddress(userVO.getUsername());
            if (!validity)
                result.add("Please enter a valid Email");
            List<String> invalidEmailDomainList = Arrays.asList(userVO.getInvalidEmailDomains().split("\\s*,\\s*"));
            for (String invalidDomain : invalidEmailDomainList) {
           	 if (userVO.getUsername().toLowerCase().contains(invalidDomain))
                    result.add("Please enter a Corporate Email address");
            }

        }

        if (userVO.getCompanyName() == null || userVO.getCompanyName() == "") {
            result.add("Please enter the CompanyName");

        }
       if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else if (!phoneNumberPattern.matcher(userVO.getPhoneNumber()).find())
            result.add("Please enter a valid PhoneNumber");

        return result;

    }
    public  List<String> demoRequestValidation(UserVO userVO) {
        List<String> result = new ArrayList<String>();
        Pattern phoneNumberPattern = Pattern.compile("^\\d{8,11}$");

        if (userVO.getUsername() == null) {
            result.add("Please enter the Email");
        } else {
            boolean validity = isValidEmailAddress(userVO.getUsername());
            if (!validity)
                result.add("Please enter a valid Email");
            List<String> invalidEmailDomainList = Arrays.asList(userVO.getInvalidEmailDomains().split("\\s*,\\s*"));
            for (String invalidDomain : invalidEmailDomainList) {
           	 if (userVO.getUsername().toLowerCase().contains(invalidDomain))
                    result.add("Please enter a Corporate Email address");
            }

        }

        if (userVO.getUsername() == null)
            result.add("Please enter the username");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else if (!phoneNumberPattern.matcher(userVO.getPhoneNumber()).find())
            result.add("Please enter a valid PhoneNumber");

        return result;

    }
}
