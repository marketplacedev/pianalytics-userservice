package com.piqube.pianalytics.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.piqube.marketplace.util.Helper;
import com.piqube.pianalytics.model.Authorities;
import com.piqube.pianalytics.model.Authority;
import com.piqube.pianalytics.model.Company;
import com.piqube.pianalytics.model.Notification;
import com.piqube.pianalytics.model.NotificationPreferences;
import com.piqube.pianalytics.model.UserCompany;
import com.piqube.pianalytics.model.Users;
import com.piqube.pianalytics.repository.NotificationPreferencesRepository;
import com.piqube.pianalytics.service.AuthoritiesService;
import com.piqube.pianalytics.service.CompanyService;
import com.piqube.pianalytics.service.NotificationService;
import com.piqube.pianalytics.service.UserCompanyService;
import com.piqube.pianalytics.service.UserService;
import com.piqube.pianalytics.util.UserServiceHelper;
import com.piqube.pianalytics.vo.NotificationModelVO;
import com.piqube.pianalytics.vo.UserVO;

/**
 * @author jenefar
 */
@RestController
@EnableRedisHttpSession
@EntityScan(basePackages="com.piqube.pianalytics")
@EnableJpaRepositories({"com.piqube.pianalytics.repository"})
public class SignupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);
    
    @Value("${invalid.email.domains}")
	String invalidEmailDomains;
    
    @Autowired
	UserService userService;
    
    @Autowired
	UserCompanyService userCompanyService;
    
    @Autowired
	NotificationPreferencesRepository notificationPreferencesRepository;
    
    @Autowired
	NotificationService notificationService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	AuthoritiesService authoritiesService;
	
    
    @Value("${piqube.verification.server}")
	String verificationServer;

	@Value("${piqube.verification.port}")
	String verificationPort;

	
	@Value("${piqube.ui.redirection.url}")
	String companyOnboardRedirectionUrl;
	

	@Value("${piqube.ui.server}")
	String UIServer;

	@Value("${piqube.ui.port}")
	String UIPort;

	

    UserServiceHelper userServiceHelper = new UserServiceHelper();

    @PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','USER')")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public
    @ResponseBody
    String getUser() throws IllegalAccessException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        LOGGER.debug("Logged in user is:{}",username);
        return username;
    }
    
    /**
	 * signup method for company onboard
	 *
	 * @param userVO
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<String> companyadminSignup(@RequestBody UserVO userVO) {

		LOGGER.debug("company onboard user signup POST called");
		userVO.setInvalidEmailDomains(invalidEmailDomains);
		List<String> errors = userServiceHelper.currentvalidateAdminSignup(userVO);

		if (errors.size() > 0)
			return new ResponseEntity<>(StringUtils.join(errors, ","), HttpStatus.BAD_REQUEST);

		String userDomain = userServiceHelper.getEmailDomain(userVO.getUsername());
		List<String> emailsList = userService.findAllEmails();

		LOGGER.debug("user domain is::::{}" + userDomain);
//		for (String email : emailsList) {
//			LOGGER.debug("email:::::" + email);
//			if (email.contains(userDomain))
//				return new ResponseEntity<>("This domain is already registered with us", HttpStatus.CONFLICT);
//		}


		Users u = userService.getUserByEmail(userVO.getUsername());

		if (u != null) {
			return new ResponseEntity<>("User with this email address already exists!", HttpStatus.CONFLICT);
		} else {


			Users user = new Users();
			user.setUsername(userVO.getUsername().toLowerCase());
			user.setPhoneNumber(userVO.getPhoneNumber());
			UserCompany userCompany = new UserCompany();
			userCompany.setCompanyName(userVO.getCompanyName());
            
			Company comp = companyService.findByCompanyName(userVO.getCompanyName());
			if (null != comp) {
				userCompany.setIsFreeText(false);
				userCompany.setMasterCompany(comp);
			} else
				userCompany.setIsFreeText(true);

			UserCompany existingCompany = userCompanyService.findByCompanyName(userCompany.getCompanyName());
			if (null != existingCompany)
				return new ResponseEntity<>("This company is already registered with us", HttpStatus.CONFLICT);

			
			UserCompany createdCompany = userCompanyService.createCompany(userCompany);
			user.setCompany(createdCompany);
			Users createdUser = userService.createUser(user);
			Set<Authorities> authorities = new HashSet<Authorities>();
			Authorities authority = new Authorities();

			authority.setUser(createdUser);
			authority.setAuthority(Authority.USER.name());
			authorities.add(authority);
			user.setAuthorities(authorities);

			NotificationPreferences preferences = new NotificationPreferences();
			preferences.setUsername(userVO.getUsername());
			preferences.setCreatedAt(new Date());
			notificationPreferencesRepository.saveAndFlush(preferences);

			sendEMail(createdUser, null, Notification.Template.ONBOARD_VERIFICATION.toString(),
					Notification.Template.ONBOARD_VERIFICATION.getTemplateCode());

			LOGGER.debug("Signup done. Email verification needs to be done for :{}", createdUser.getUsername());
			
			
			
		}
		return null;
	}
	
	@RequestMapping(value = "/user/email-verify", method = RequestMethod.GET)
	public RedirectView redirectView(@RequestParam(value = "key", required = false) String key,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "companyName", required = false) String companyName) {

		LOGGER.debug("company onboard create account GET is called...");
		Users user = userService.findUserByUUID(key);
		RedirectView redirectView = new RedirectView();
		if (null != user) {
			
			userService.updateUser(user);

			LOGGER.debug("company onboard email is successfully verified for user with uuid:{}", key);

			redirectView.setUrl(companyOnboardRedirectionUrl+"/user/new-onboarding?key=" + key);

			LOGGER.debug("redirecting to login...");

			System.out.println("redirect view:::" + redirectView.getUrl());
			return redirectView;
		} else {
			LOGGER.debug("create account key is invalid -{}", key);
			throw new IllegalArgumentException("create account key is invalid");
		}

	}
	

	/**
	 * signup method for company update
	 * 
	 * @param userVO
	 */
	@RequestMapping(value = "/create-account/{key}", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public void adminAccount(@PathVariable String key, @RequestBody UserVO userVO) {
		LOGGER.debug("company onboard update account PUT is called...");
		Users user = userService.findUserByUUID(key);
		
		if (user == null) {
			throw new IllegalArgumentException("Email verification url is wrong!");
		} else {

			List<String> errors = userServiceHelper.validateAccount(userVO);
			if (errors.size() > 0)
				throw new IllegalArgumentException(StringUtils.join(errors, ","));
			user.setEmailVerified(true);
			user.setAccountEnabled(true);
			user.setTimezone(userVO.getTimezone());
			user.setFirstName(userVO.getFirstName());
			user.setLastName(userVO.getLastName());
			user.setPassword(new BCryptPasswordEncoder().encode(userVO.getPassword()));
			
			userService.updateUser(user);
			
			
			
		}
		sendEMail(user, null, Notification.Template.ADMIN_REGISTERED.toString(),
				Notification.Template.ADMIN_REGISTERED.getTemplateCode());
		LOGGER.debug("Signup done. Email verification needs to be done for :{}");

	}
	
	@RequestMapping(value = "/user/forgot-password", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void forgotPassword(@RequestBody UserVO userVO) throws IllegalAccessException {

		LOGGER.debug("forgot password is called...");
		Users user = userService.getUserByEmail(userVO.getUsername());
		if (null == user)
			throw new IllegalArgumentException("No user with this email address exists. Please enter the email address with which you have signed up");

		else {
			// send reset password link
			String passwordUrl = "/user/reset-password?key=" + user.getuUID();
			LOGGER.debug("The password URL is {}", passwordUrl);
			sendEMail(user, null, Notification.Template.FORGOT_PASSWORD.name(),
					Notification.Template.FORGOT_PASSWORD.getTemplateCode());
			LOGGER.debug("exiting forgot password POST...");

		}

	}

	@RequestMapping(value = "/user/reset-password", method = RequestMethod.GET)
	public RedirectView changePassword(@RequestParam(value = "key", required = false) String key) {
		Users user = userService.findUserByUUID(key);
		RedirectView redirectView = new RedirectView();

		if (null != user) {
			LOGGER.debug("Reset Password Redirect url::::" + "http://" + UIServer + ":" + UIPort + "/set-password?key="
					+ key);
			redirectView.setUrl("http://" + UIServer + ":" + UIPort + "/set-password?key=" + key);
			LOGGER.debug("Password reset url is correct. Now page to reset password should be rendered");
			System.out.println("redirect view:::" + redirectView.getUrl());
			return redirectView;
		} else {
			LOGGER.debug("reset password url is wrong with key:{}", key);
			throw new IllegalArgumentException("reset password url is wrong!");
		}    
	}

	@RequestMapping(value = "/user/reset-password", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void resetPassword(@RequestBody UserVO userVO) {

		LOGGER.debug("Going to reset user password");
		Users user = userService.findUserByUUID(userVO.getUuid());
		if (userVO.getPassword() != null && !"".equals(userVO.getPassword())) {
			if (null != user) {
				user.setPassword(new BCryptPasswordEncoder().encode(userVO.getPassword()));
				userService.updateUser(user);
				LOGGER.debug("Password is successfully reset for {}", user.getUsername());
			} else
				throw new IllegalArgumentException("No record found to update the password");
		} else
			throw new IllegalArgumentException("Please enter the new password");

	}
	
	public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email); 
        return m.matches();
    }
	
	 /**
     * update password or phone number or email
     *
     * @param userVO
     * @param String 
     * @throws IllegalAccessException
     */
    @RequestMapping(value = "/user/{email:.+}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void updateUser(@PathVariable String email, @Valid @RequestBody UserVO userVO, Object String) throws IllegalAccessException {

        LOGGER.debug("User update PUT is called...");

        if (null == userVO)
            throw new IllegalArgumentException("Please enter values to be updated");
        else {
        	
            String username = userServiceHelper.getLoggedInUser();
            String tempUserName = userVO.getUsername();
        	Set<Authorities> authorities=  authoritiesService.findByUsername(username);
        	
            if (username.equals(email)) {
                Users currentUser = userService.getUserByEmail(username);
                
                if (null == currentUser)
                    throw new IllegalArgumentException("No user record found to update details");
                else {
                	if (userVO.getUsername() == null) {
                		throw new IllegalArgumentException("Please enter the Email");
                    } else {
                        boolean validity = isValidEmailAddress(userVO.getUsername());
                        if (!validity)
                        	throw new IllegalArgumentException("Please enter a valid Email");
                        userVO.setInvalidEmailDomains(invalidEmailDomains);
                        List<String> invalidEmailDomainList = Arrays.asList(userVO.getInvalidEmailDomains().split("\\s*,\\s*"));
                        for (String invalidDomain : invalidEmailDomainList) {
                       	 if (userVO.getUsername().toLowerCase().contains(invalidDomain))
                       		throw new IllegalArgumentException("Please enter a Corporate Email address");
                        }
                       	String userDomain = userServiceHelper.getEmailDomain(userVO.getUsername());
                		List<String> emailsList = userService.findAllEmails();

                		LOGGER.debug("user domain is::::{}" + userDomain);
                		for (String email1 : emailsList) {
                			
                			if (email1.contains(userDomain))
                				throw new IllegalArgumentException("This domain is already registered with us");
                		}

                		Users u = userService.getUserByEmail(userVO.getUsername());

                		if (u != null) {
                			throw new IllegalArgumentException("User with this email address already exists!");
                		} else {
                			currentUser.setUsername(userVO.getUsername());
                		}

                    }

                    if (userVO.getPassword() != null && !"".equals(userVO.getPassword())) {
                    
                        if (userVO.getPassword().length() < 8){
                            throw new IllegalArgumentException("Please enter atleast 8 digits password");
                        }else{
                        	String test_passwd = userVO.getCurrentPassword();
                    		String test_hash = currentUser.getPassword();
                    		String computed_hash = userServiceHelper.hashPassword(test_passwd);
                    		String compare_test = userServiceHelper.checkPassword(test_passwd, test_hash)
                    				? "Passwords Match" : "Passwords do not match";
                    			String compare_computed = userServiceHelper.checkPassword(test_passwd, computed_hash)
                    				? "Passwords Match" : "Passwords do not match";
                    			
                    		if(compare_test.equals("Passwords Match"))	{
                            currentUser.setPassword(new BCryptPasswordEncoder().encode(userVO.getPassword()));
                    		}else{
                    			throw new IllegalArgumentException("Please enter correct current password");
                    		}
                           
                        }
                    }
                    if (userVO.getPhoneNumber() != null && !"".equals(userVO.getPhoneNumber()) && !userVO.getPhoneNumber().equals(currentUser.getPhoneNumber())) {
                        Pattern pattern = Pattern.compile("^\\d{8,11}$");
                        if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                            throw new IllegalArgumentException("Please enter a valid phone number");
                        }
                        currentUser.setPhoneNumber(userVO.getPhoneNumber());
//                        currentUser.setIsMobileVerified(0);
//                        currentUser.setOtp(null);
//                        currentUser.setOtpAt(null);
                    }
                    
                    if(userVO.getUsername()!=null){
                    	for (Authorities authority : authorities) {
                			authority.setUser(currentUser);
                			authoritiesService.saveAuthority(authority);
                		}
                    	userService.updateUser(currentUser);
                    
                    }
                    
                    NotificationPreferences notificationPreferences = notificationPreferencesRepository.findByUsername(email);
            		notificationPreferences.setUsername(userVO.getUsername());
            		notificationPreferencesRepository.save(notificationPreferences);

                    
                }
            } else
                throw new IllegalAccessException("You are not authorized to edit details of :" + email + ". Only logged in users can edit their details");
        }
    }
	
	public void sendEMail(Users createdUser, Users admin, String templateType, int templateCode) {

		NotificationModelVO notificationModel = new NotificationModelVO();
		Notification notification = new Notification();
		notificationModel.setUuid(createdUser.getuUID());

		notificationModel.setEmailVerificationServer(verificationServer.trim());
		notificationModel.setEmailVerificationServerPort(verificationPort.trim());
		LOGGER.debug(notificationModel.getEmailVerificationServer() + "verficationserver");
		LOGGER.debug(notificationModel.getEmailVerificationServerPort() + "verficationport");

		if (templateType.equals(Notification.Template.ADMIN_REGISTERED.name())) {
			if (createdUser.getFirstName() != null && !"".equals(createdUser.getFirstName()))
				notificationModel.setFirstName(createdUser.getFirstName());
			if (createdUser.getLastName() != null && !"".equals(createdUser.getLastName()))
				notificationModel.setLastName(createdUser.getLastName());
			if (createdUser.getUsername() != null && !"".equals(createdUser.getUsername()))
				notificationModel.setEmail(createdUser.getUsername());
			if (createdUser.getCompany() != null && !"".equals(createdUser.getCompany()))
				notificationModel.setCompanyName(createdUser.getCompany().getCompanyName());
			if (createdUser.getPhoneNumber() != null && !"".equals(createdUser.getPhoneNumber()))
				notificationModel.setPhoneNumber(createdUser.getPhoneNumber());


		} else {
			try {
				if (createdUser.getFirstName() != null && !"".equals(createdUser.getFirstName()))
					notificationModel.setFirstName(URLEncoder.encode(createdUser.getFirstName(), "UTF-8"));
				if (createdUser.getLastName() != null && !"".equals(createdUser.getLastName()))
					notificationModel.setLastName(URLEncoder.encode(createdUser.getLastName(), "UTF-8"));
				if (createdUser.getUsername() != null && !"".equals(createdUser.getUsername()))
					notificationModel.setEmail(URLEncoder.encode(createdUser.getUsername(), "UTF-8"));
				if (createdUser.getCompany() != null && !"".equals(createdUser.getCompany()))
					notificationModel
							.setCompanyName(URLEncoder.encode(createdUser.getCompany().getCompanyName(), "UTF-8"));
				if (createdUser.getPhoneNumber() != null && !"".equals(createdUser.getPhoneNumber()))
					notificationModel.setPhoneNumber(URLEncoder.encode(createdUser.getPhoneNumber(), "UTF-8"));

				if (null != admin) {
					if (admin.getFirstName() != null && !"".equals(admin.getFirstName()))
						notificationModel.setAdminFirstName(URLEncoder.encode(admin.getFirstName(), "UTF-8"));
					if (admin.getLastName() != null && !"".equals(admin.getLastName()))
						notificationModel.setAdminLastName(URLEncoder.encode(admin.getLastName(), "UTF-8"));
					GrantedAuthority authority = ((UserDetails) SecurityContextHolder.getContext().getAuthentication()
							.getPrincipal()).getAuthorities().iterator().next();
					notificationModel.setAuthority(authority.getAuthority());
				}
				List<String> recipients = new ArrayList<String>();
				recipients.add(createdUser.getUsername());
				notification.setRecipients(recipients);

			} catch (UnsupportedEncodingException e) {
				LOGGER.error("error in encoding");
			}

		}
		String model = Helper.convertObjectToJson(notificationModel);

		notification.setEmail(createdUser.getUsername());
		notification.setTemplate(templateCode);
		notification.setTemplateType(templateType);
		notification.setModel(model);
		notification.setUser_id(createdUser.getId());

		
		notificationService.sendNotification(notification);
	}
}
