package com.piqube.pianalytics.controller;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.piqube.pianalytics.model.AccessKeys;
import com.piqube.pianalytics.model.SubscriptionPackages;
import com.piqube.pianalytics.model.UserSubscription;
import com.piqube.pianalytics.model.Users;
import com.piqube.pianalytics.repository.UserSubscriptionRepository;
import com.piqube.pianalytics.service.AccessKeyService;
import com.piqube.pianalytics.service.UserService;
import com.piqube.pianalytics.util.UserServiceHelper;
import com.piqube.pianalytics.vo.AccessKeyVO;
import com.piqube.pianalytics.vo.UserVO;

/**
 * @author jenefar
 */
@RestController
@EnableRedisHttpSession
@EntityScan(basePackages="com.piqube.pianalytics")
@EnableJpaRepositories({"com.piqube.pianalytics.repository"})
public class UserController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);
	
	UserServiceHelper userServiceHelper = new UserServiceHelper();

    
	@Autowired
	UserService userService;
	
	@Autowired
	AccessKeyService accessKeyService;

	@Autowired
	UserSubscriptionRepository userSubscriptionRepository;
	
	@RequestMapping(value = "/user/keys", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	 public void createAccessKeys(@RequestBody AccessKeyVO accessKeyVO) {

		   LOGGER.debug("=====creating keys=====");
	       String username=userServiceHelper.getLoggedInUser();

	       Users user=userService.getUserByEmail(username);
	       AccessKeys accessKey=accessKeyService.getUserByUserId(user);
	       LOGGER.debug("user id:"+user.getId());

	       AccessKeys accessKeys=new AccessKeys();
	       UserSubscription userSubscription=new UserSubscription();
	       if(accessKey==null){
	    	   UUID uuid = UUID.randomUUID();
	    	   accessKeys.setAccessKey(uuid.toString());
	    	   accessKeys.setTrial(accessKeyVO.getIsTrail());
	    	   accessKeys.setUser(user);
	    	   accessKeys.setCreatedAt(new Date());
	    	   AccessKeys accesKey=accessKeyService.updateAccessKey(accessKeys);
	    	   userSubscription.setAccessKey(accesKey);
	    	   userSubscription.setCallsPerDay(5);
	    	   userSubscription.setCallsPerMonth(10);
	    	   userSubscription.setCreatedAt(new Date());
	    	   userSubscription.setCreatedBy(user);
	    	   userSubscription.setRateLimit(10);
	    	   userSubscription.setRemainingCallsPerDay(5);
	    	   userSubscription.setRemainingCallsPerMonth(10);
	    	   userSubscription.setScoreBreakupAvailability(5);
	    	   userSubscription.setSocialProfileAvailability(5);
	    	   userSubscription.setUser(user);
	    	   userSubscription.getScoreBreakupAvailability();
	    	   userSubscriptionRepository.save(userSubscription);
	       }else{
	    	   if(accessKeyVO.getIsTrail().equals(true)){
	    		   accessKey.setTrial(true);
	    	   }else{
	    		   accessKey.setTrial(false);
	    	   }
	    	   accessKeyService.updateAccessKey(accessKey);
	       }
	     
	}

	
	@RequestMapping(value = "/user/keys", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	 public String getAccessKeys() {

        LOGGER.debug("user get method is called:");
        String username = userServiceHelper.getLoggedInUser();
        Users user = userService.getUserByEmail(username);
        AccessKeys accessKey=accessKeyService.getUserByUserId(user);
        
        if (null == accessKey&&user==null){
            throw new IllegalArgumentException("No User with the email id exists");

        } else {
        	UserSubscription userSubscription=userSubscriptionRepository.findByAccesskey(accessKey);
            LOGGER.debug("User found is:" + user.getFirstName());
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            
            
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode json = null;
            try {
                json = (ObjectNode) mapper.readTree(gson.toJson(userSubscription));
                
            } catch (IOException e) {
                e.printStackTrace();
            }

            LOGGER.debug("GET method completed!");
            return json.toString();

        }
    }
	
	@RequestMapping(value = "/user/keys", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	 public void generateAccessKeys(@RequestBody AccessKeyVO accessKeyVO) {
		 LOGGER.debug("=====creating keys=====");
	       String username=userServiceHelper.getLoggedInUser();

	       Users user=userService.getUserByEmail(username);
	       AccessKeys accessKey=accessKeyService.getUserByUserId(user);
	       if(accessKey!=null){
	    	   if(accessKeyVO.getGenerateKey()!=null){
	       if(accessKeyVO.getGenerateKey().equals(true)){
	    	  
	       LOGGER.debug("user id:"+user.getId());
	       UUID uuid = UUID.randomUUID();
	       
	       accessKey.setAccessKey(uuid.toString());
	       accessKey.setTrial(accessKeyVO.getIsTrail());
	       AccessKeys accesKey=accessKeyService.updateAccessKey(accessKey);
	       UserSubscription userSubscription=userSubscriptionRepository.findByUserId(user);
	       userSubscription.setAccessKey(accesKey);
	       userSubscriptionRepository.save(userSubscription);
	       }
	       }
	    	   if(accessKeyVO.getIsEnabled()!=null){

	       if(accessKeyVO.getIsEnabled().equals(true)){
	    	   UserSubscription userSubscription=userSubscriptionRepository.findByUserId(user);
	    	   userSubscription.setLocked(accessKeyVO.getIsEnabled());
	    	   userSubscriptionRepository.save(userSubscription);
	       }else{
	    	   UserSubscription userSubscription=userSubscriptionRepository.findByUserId(user);
	    	   userSubscription.setLocked(accessKeyVO.getIsEnabled());
	    	   userSubscriptionRepository.save(userSubscription);
	       }
	    	   }
	       }else{
	    	   throw new IllegalArgumentException("Please provide details");

	       }
	}
	
	
}


